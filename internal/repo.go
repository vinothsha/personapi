package internal

type Repo interface {
	Save(*Person) *CommonResponse
	Get(int) *Person
}
