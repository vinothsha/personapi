package internal

type Person struct {
	Id       int     `json:"id"`
	Name     string  `json:"name" validate:"required,min=2"` // name is required
	Email    string  `json:"email"`
	City     string  `json:"city"`
	Age      int     `json:"age"`
	Balance  float32 `json:"balance"`
	Gender   string  `json:"gender"`
	EyeColor string  `json:"eyeColor"`
}

type CommonResponse struct {
	Status bool        `json:"status"`
	Data   interface{} `json:"data"`
}
