package internal

type repo struct{}

func (r *repo) Save(req *Person) *CommonResponse {
	return &CommonResponse{}
}

var data = Person{
	Id:       2,
	Balance:  3239.81,
	Age:      27,
	EyeColor: "blue",
	Name:     "Ollie",
	Gender:   "female",
	Email:    "olliehorn@stralum.com",
	City:     "Bahamas",
}

func (r *repo) Get(req int) *Person {
	return &data
}

func NewService() Repo {
	return &repo{}
}
