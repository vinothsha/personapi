package handler

import "github.com/gin-gonic/gin"

type IPerson interface {
	CreateNewPerson(*gin.Context)
	GetPersons(*gin.Context)
}
