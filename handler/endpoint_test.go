// main_test.go
package handler

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/vinothsha/personpg/internal"
)

func TestGetDataHandler(t *testing.T) {
	// Save some data to be retrieved by GET request
	savedData := internal.Person{
		Name: "Ollie",
		Age:  27,
	}
	var obj = NewHandler(internal.NewService())

	// Create a new Gin router and add the GET endpoint
	gin.SetMode(gin.ReleaseMode)

	r := gin.Default()
	r.GET("/get", obj.GetPersons)

	// Perform a GET request to the "/api/data" endpoint
	req, err := http.NewRequest("GET", "/get", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	var data internal.Person

	err = json.NewDecoder(rr.Body).Decode(&data)
	if err != nil {
		t.Errorf("error decoding response: %v", err)
	}

	// Add assertions to verify the response data
	if data.Name != savedData.Name {
		t.Errorf("expected name to be 'John Doe', got %s", data.Name)
	}
	if data.Age != savedData.Age {
		t.Errorf("expected age to be 30, got %d", data.Age)
	}
}
