package handler

import (
	"fmt"
	"net/http"

	"gitlab.com/vinothsha/personpg/internal"

	"github.com/gin-gonic/gin"
)

type service struct {
	repo internal.Repo
}

func (s *service) CreateNewPerson(*gin.Context) {
	s.repo.Save(&internal.Person{})
}

func (s *service) GetPersons(c *gin.Context) {
	res := s.repo.Get(2)
	fmt.Println(res)
	c.JSON(http.StatusOK, res)
}

func NewHandler(s internal.Repo) IPerson {
	return &service{s}
}
