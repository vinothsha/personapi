package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/vinothsha/personpg/handler"
	"gitlab.com/vinothsha/personpg/internal"

	"github.com/gin-gonic/gin"
	"github.com/rs/cors"
)

var srv *http.Server

func main() {
	var repoconnect = internal.NewService()
	handle := handler.NewHandler(repoconnect)

	router := routerInitailize(handle)

	//cors setting, default allows all -> *
	allowedHeaders := []string{"*"}
	allowedOrigins := []string{"*"}
	allowedMethods := []string{"GET", "POST", "OPTIONS", "DELETE"}
	corsOption := cors.New(cors.Options{
		AllowedOrigins: allowedOrigins,
		AllowedHeaders: allowedHeaders,
		AllowedMethods: allowedMethods,
	})

	handler := corsOption.Handler(router)

	srv = &http.Server{
		Addr:         ":8080",
		Handler:      handler,
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
	}

	// Start the server
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("failed to serve")
		}
	}()

	log.Printf("Server started at %s", "8080")
	Gracefullstop()

}

func routerInitailize(handle handler.IPerson) *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(gin.Recovery())
	router.POST("/save", handle.CreateNewPerson)
	router.GET("/get", handle.GetPersons)
	return router
}

func Gracefullstop() {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	// Create a context with a timeout for graceful shutdown
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Shutdown the server gracefully
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("failed to shutdown gracefully: %v", err)
	}
}
