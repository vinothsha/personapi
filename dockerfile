FROM golang:1.20 as builder

WORKDIR /app

# Copy the Go module files to the working directory
COPY go.mod go.sum ./

# Download and cache Go dependencies
RUN go mod download

COPY . .

RUN CGO_ENABLED=0  GOOS=linux go build -a -installsuffix cgo -o myapp cmd/main.go

FROM scratch

WORKDIR /app

COPY --from=builder /app/myapp /test

EXPOSE 8080

CMD ["/test"]
